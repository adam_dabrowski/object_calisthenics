package objectcalisthenics.vendingmachine.money;

public class ChangeCalculator {

    public Coins neededCoins(Money money) {
        Coins neededCoins = new Coins();
        collect(neededCoins, money, Denominator.greatest());
        return neededCoins;
    }

    private void collect(Coins neededCoins, Money money, Denominator denominator) {
        while (sumWithAdded(neededCoins, denominator).lessOrEqual(money)) {
            neededCoins.add(coinOf(denominator));
        }
        if (haveSameValue(neededCoins, money))
            return;
        collect(neededCoins, money, denominator.next());
    }

    private boolean haveSameValue(Coins neededCoins, Money money) {
        return summedValue(neededCoins).hasSameValue(money);
    }

    private Money sumWithAdded(Coins neededCoins, Denominator denominator) {
        return summedValue(neededCoins).add(coinOf(denominator));
    }

    private Money summedValue(Coins neededCoins) {
        return neededCoins.summedValue();
    }

    private Coin coinOf(Denominator denominator) {
        return Coin.valueOf(denominator);
    }

}
