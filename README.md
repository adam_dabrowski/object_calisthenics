Object Calisthenics Rules
=========================

1. One level of indentation per method
2. Don’t use the ELSE keyword
3. Wrap all primitives and Strings
4. First class collections
5. One dot per line
6. Don’t abbreviate
7. Keep all entities small
8. No classes with more than two instance variables
9. No getters/setters/properties


VendingMachineKata
==================

1. Maszyna zawiera produkty,
2. Produkty mogą być różnych typów (np. napój Cola 0.25l, batonik czekoladowy, woda mineralna 0.33l, itd.)
3. Produkty są ułożone na półkach,
5. Każdy półka ma przypisaną cenę,
6. Maszyna posiada wyświetlacz,
7. Jeżeli wybierzemy numer półki na wyświetlaczu pojawia się cena produktu,
8. Produkty można kupić wrzucając do automatu monety (nominały: 5, 2, 1),
9. Wrzucając kolejne monety na wyświetlaczu aktualizuje się kwota która należy jeszcze wrzucić,
10. Jeżeli wybierzemy numer półki i wrzucimy odpowiednią kwotę to otrzymujemy produkt i resztę,
11. Jeżeli nie wrzucimy odpowiedniej kwoty musimy wcisnąć “Anuluj”, żeby otrzymać pieniądze z powrotem,
12. Jeżeli maszyna nie może wydać reszty wyświetla komunikat i zwraca wrzucone monety,
13. Przy wydawaniu reszty maszyna może korzystać tylko z monet które posiada (nie drukujemy pieniędzy ;) )

Podpowiedzi nazw dla klas: Slot/Shelf, Display, Product/ProductInstance, Coin, CoinDispenser, Storage, ProductDispenser.